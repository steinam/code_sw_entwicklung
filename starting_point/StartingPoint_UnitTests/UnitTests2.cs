﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;

namespace StartingPoint
{
   
   
  [TestFixture] 
  public class UnitTests2 { 
    // Movies 
    Movie m_Cinderella; 
    Movie m_StarWars; 
    Movie m_Gladiator; 
    Movie m_BruceAlmighty; 
 
    // Rentals 
    Rental m_Rental1; 
    Rental m_Rental2; 
    Rental m_Rental3; 
    Rental m_Rental4; 
 
    // Customers 
    Customer m_MickeyMouse; 
    Customer m_DonaldDuck; 
    Customer m_BillClinton; 
    Customer m_GeorgeBush; 
 
    [SetUp] 
    public void Init()  
    { 
      // Create movies 
      m_Cinderella = new Movie("Cinderella", PriceCodes.Childrens); 
      m_StarWars = new Movie("Star Wars", PriceCodes.Regular); 
      m_Gladiator = new Movie("Gladiator", PriceCodes.NewRelease); 
      m_BruceAlmighty = new Movie("Bruce Almighty", PriceCodes.NewRelease); 
 
      // Create rentals 
      m_Rental1 = new Rental(m_Cinderella, 5); 
      m_Rental2 = new Rental(m_StarWars, 5); 
      m_Rental3 = new Rental(m_Gladiator, 5); 
      m_Rental4 = new Rental(m_BruceAlmighty, 365); 
 
      // Create customers 
      m_MickeyMouse = new Customer("Mickey Mouse"); 
      m_DonaldDuck = new Customer("Donald Duck"); 
      m_BillClinton = new Customer("Bill Clinton"); 
      m_GeorgeBush = new Customer("George Bush"); 
    } 
 
    [Test] 
    public void TestGeorgeBushCustomer()  
    { 
      // Test Name property 
      Assert.AreEqual("George Bush", m_GeorgeBush.Name); 
 
      // Test AddRental() method - set up for test 
      m_GeorgeBush.AddRental(m_Rental2); 
      m_GeorgeBush.AddRental(m_Rental3); 
      m_GeorgeBush.AddRental(m_Rental3); 
      m_GeorgeBush.AddRental(m_Rental4); 
      m_GeorgeBush.AddRental(m_Rental4);
 
      // Test the Statement() method 
      string theResult = m_GeorgeBush.Statement(); 
 
      // Parse the result 
      char[] delimiters = "\n\t".ToCharArray(); 
      string[] results = theResult.Split(delimiters); 
 
      Assert.AreEqual("Rental record for George Bush\n" + 
        "\tStar Wars\t6.5\n" + 
        "\tGladiator\t15\n" + 
        "\tGladiator\t15\n" + 
        "\tBruce Almighty\t1095\n" + 
        "\tBruce Almighty\t1095\n" + 
        "Amount owed is 2226.5\n" + 
        "You earned 9 frequent renter points.", theResult); 
      Console.WriteLine(theResult); 
    } 
  } 
} 
