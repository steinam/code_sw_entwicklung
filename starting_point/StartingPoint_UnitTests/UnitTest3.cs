﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;

namespace StartingPoint
{
    [TestFixture]
    public class UnitTests3
    {
        Movie _ToyStory;
        Movie _MonstersINC;
        Movie _PayItForward;
        Movie _TheMatrix;

        // Customers 
        Customer _GeorgeBush;

        [TestFixtureSetUp]
        public void TestFixtureSetup()
        {
            // Create movies 
            _ToyStory = new Movie("Toy Story", PriceCodes.Childrens);
            _MonstersINC = new Movie("Monsters, INC.", PriceCodes.Regular);
            _PayItForward = new Movie("Pay It Forward", PriceCodes.NewRelease);
            _TheMatrix = new Movie("The Matrix", PriceCodes.NewRelease);

            // Create customers 
            _GeorgeBush = new Customer("George Bush");

            _GeorgeBush.AddRental(new Rental(_MonstersINC, 14));
            _GeorgeBush.AddRental(new Rental(_PayItForward, 21));
            _GeorgeBush.AddRental(new Rental(_PayItForward, 21));
        }

        [Test]
        public void TestGeorgeBushAgain()
        {
            // Test Name property 
            Assert.AreEqual("George Bush", _GeorgeBush.Name);

            // Test the Statement() method 
            string theResult = _GeorgeBush.Statement();

            // Parse the result 
            char[] delimiters = "\n\t".ToCharArray();
            string[] results = theResult.Split(delimiters);

            Assert.AreEqual("Rental record for George Bush\n" +
              "\tMonsters, INC.\t20\n" +
              "\tPay It Forward\t63\n" +
              "\tPay It Forward\t63\n" +
              "Amount owed is 146\n" + "You earned 5 frequent renter points.", theResult);
            Console.WriteLine(theResult);
        }

        [Test]
        public void TestMoviePriceCodeChanged()
        {
            Assert.AreEqual(PriceCodes.Childrens, _ToyStory.PriceCode);
            _ToyStory.PriceCode = PriceCodes.NewRelease;
            Assert.AreEqual(PriceCodes.NewRelease, _ToyStory.PriceCode);
        }

        [Test]
        public void TestMovieTitle()
        {
            Assert.AreEqual("Toy Story", _ToyStory.Title);
            Assert.AreEqual("StartingPoint.Movie", _ToyStory.ToString());
        }

        [Test]
        public void TestMovieFromRental()
        {
            Rental _Rental = new Rental(_MonstersINC, 5);
            Assert.AreEqual(5, _Rental.DaysRented);
            Assert.AreEqual(PriceCodes.Regular, _Rental.Movie.PriceCode);
            Assert.AreEqual("Monsters, INC.", _Rental.Movie.Title);
        }

        [Test]
        public void TestTwoFrequentRenterPointsFromOneMovie()
        {
            Customer _Wala = new Customer("Wala");
            _Wala.AddRental(new Rental(new Movie("The Gods Must Be Crazy 2",
            PriceCodes.NewRelease), 10));
            Assert.AreEqual("Rental record for Wala\n\tThe Gods Must Be "
                + "Crazy 2\t30\nAmount owed is 30\n You earned 2 frequent "
                + "renter points."
                , _Wala.Statement());
        }

        [Test]
        public void TestSomething()
        {
            Customer _Punk = new Customer("Chump");
            _Punk.AddRental(new Rental(new Movie("Bozo", PriceCodes.Childrens),
                10));
            Assert.AreEqual("Chump", _Punk.Name);
            Assert.AreEqual
              ("Rental record for Chump\n\tBozo\t12\nAmount owed is 12\n"
              + "You earned 1 frequent renter points."
              , _Punk.Statement());
            _Punk.AddRental(new Rental(new Movie("Banana Splits",
              PriceCodes.Childrens), 10));
            Assert.AreEqual
              ("Rental record for Chump\n\tBozo\t12\n"
              + "\tBanana Splits\t12\nAmount owed is 24\n"
              + "You earned 2 frequent renter points."
              , _Punk.Statement());
        }
    }

}
