﻿using System;
using NUnit.Framework;
using Stacker;
using NUnit.Framework.Constraints;

namespace TestStacker
{

    [TestFixture]
    class TestStacker
    {
        //Arrange string testDate = "20121123120122";

        //Act
        //ActualValueDelegate<object> testDelegate = () => testDate.FromPrinergyDateTime();

        //Assert
        //Assert.That(testDelegate, Throws.TypeOf<FormatException>());
         


        [Test]
        public void test6()
        {
            // Method intentionally left empty.

            StackClass myStack = new StackClass();
            //so
            Assert.Throws<InvalidOperationException>(() =>myStack.pop());
            


            //oder so
            //bracuth aber Rückgabe-Objekt
            //Act
            //ActualValueDelegate<object> testDelegate = () => myStack.pop();

            //Assert
            //Assert.That(testDelegate, Throws.TypeOf<InvalidOperationException>());
        }
    }
}
